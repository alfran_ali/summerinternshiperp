<!DOCTYPE html>
<%@page import="utilities.StringFormatter"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="../plugins/ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<script src="../dist/js/validate.min.js"></script>
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script>
	var constraints = 

			{

				
				first_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid first name", {
										text : value
									});
						}
					}
				},
				middle_name : {
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Middle Name", {
										text : value
									});
						}
					}
				},
				last_name : {
					format : {
						pattern : /^[a-zA-Z']*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Last Name", {
										text : value
									});
						}
					}
				},
				mobile : {
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid mobile number", {
										text : value
									});

						}
					}
				},
				email : {
					// Email is 
					presence : true,
					// and must be an email (duh)
					email : true
				},

				guardian_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' ]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not valid guardian name", {
										text : value
									});
						}
					}
				},
				guardian_contact : {
					presence : true,
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Mobile Number", {
										text : value
									});
						}
					}
				},
			  guardian_email : {
				// Email is 
               presence : true,
				// and must be an email (duh)
               email: true
				},
				guardian_address : {
                    presence : true,
				},
			
				father_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' ]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not valid father name", {
										text : value
									});
						}
					}
				},
				father_contact : {
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Mobile Number", {
										text : value
									});
						}
					}
				},

				mother_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' ]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not valid for mother name", {
										text : value
									});
						}
					}
				},
				
				mother_contact : {
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid mobile number", {
										text : value
									});

						}
					}
				},
			
			
				permanent_address : {
					presence : true
				},
				local_address : {
					presence : true
				},
				hostel_address : {
					
				},
				hostel_room:
					{
					
					}
			};
</script>
<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student Registration <small>Data and documents verification</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>
			<!--MODAL-->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<form id="update">
							<div class="modal-body">

								<table id="modal_table"
									class="table table-bordered table-striped">
									<tbody>

										<tr>
											<td>Registration Id</td>
											<td id="registration_id"></td>
										</tr>
										<tr>
											<td></td>
											<td><strong>Current Data</strong></td>
											<td><strong>Updated Data</strong></td>
										</tr>

										<tr>
											<td>Name</td>
											<td id="name"></td>
										</tr>

										<tr>
											<td>First Name</td>
											<td id="first_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="first_name"
														id="update_first_name">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
										<tr>
											<td>Middle Name</td>
											<td id="middle_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="middle_name"
														id="update_middle_name">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
											<td>Last Name</td>
											<td id="last_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="last_name"
														id="update_last_name">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
											<td>Category</td>
											<td id="category"></td>
										</tr>

										<tr>
											<td>State</td>
											<td id="state"></td>
										</tr>
										<tr>
											<td>Mobile</td>
											<td id="mobile"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="mobile"
														id="update_mobile">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Email</td>
											<td id="email"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="email"
														id="update_email">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Date Of Birth</td>
											<td id="date_of_birth"></td>
										</tr>
										<tr>
											<td>Program Allocated</td>
											<td id="program_allocated"></td>
										</tr>


										<tr>
											<td>Physically Disabled</td>
											<td id="physically_disabled"></td>
										</tr>

										<tr>
											<td>Gender</td>
											<td id="gender"></td>
										</tr>

										<tr>
											<td>Guardian Name</td>
											<td id="guardian_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="guardian_name" id="update_guardian_name">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Guardian Contact</td>
											<td id="guardian_contact"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="guardian_contact" id="update_guardian_contact">
													<div class="messages"></div>
												</div></td>

										</tr>

										<tr>
											<td>Guardian Email</td>
											<td id="guardian_email"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="guardian_email" id="update_guardian_email">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
											<td>Guardian Address</td>
											<td id="guardian_address"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="guardian_address" id="update_guardian_address">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
											<td>Father Name</td>
											<td id="father_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="father_name"
														id="update_father_name">
													<div class="messages"></div>
												</div></td>
										</tr>



										<tr>
											<td>Father Contact</td>
											<td id="father_contact"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="father_contact" id="update_father_contact">
													<div class="messages"></div>

												</div></td>
										</tr>

										<tr>
											<td>Mother Name</td>
											<td id="mother_name"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="mother_name"
														id="update_mother_name">
													<div class="messages"></div>

												</div></td>
										</tr>
										<tr>
											<td>Mother Contact</td>
											<td id="mother_contact"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="mother_contact" id="update_mother_contact">
													<div class="messages"></div>

												</div></td>
										</tr>

										<tr>
											<td>Nationality</td>
											<td id="nationality"></td>
										</tr>


										<tr>
											<td>Permanent Address</td>
											<td id="permanent_address"></td>
											<td><div class="form-group">
													<textarea class="form-control" type="text"
														name="permanent_address" id="update_permanent_address"></textarea>
													<div class="messages"></div>

												</div></td>
										</tr>

										<tr>
											<td>Local Address</td>
											<td id="local_address"></td>
											<td><div class="form-group">
													<textarea class="form-control" 
														name="local_address" id="update_local_address"></textarea>
													<div class="messages"></div>

												</div></td>
										</tr>

										<tr>
											<td>Hosteller</td>
											<td id="hosteller"></td>
											<td><div class="form-group">
													<input  type="radio" name="hosteller"
														id="update_hosteller_yes"><label>Yes</label> <input
														type="radio" name="hosteller" id="update_hosteller_no"><label>No</label>


												</div></td>
										</tr>

										<tr>
											<td>Hostel Address</td>
											<td id="hostel_address"></td>
											<td><div class="form-group">
													<input class="form-control" type="text"
														name="hostel_address" id="update_hostel_address"/>
													<div class="messages"></div>

												</div></td>
										</tr>

										<tr>
											<td>Hostel Room</td>
											<td id="hostel_room"></td>
											<td><div class="form-group">
													<input class="form-control" type="text" name="hostel_room"
														id="update_hostel_room">
													<div class="messages"></div>
												</div></td>
										</tr>


										<tr>
											<td>Semester</td>
											<td id="semester"></td>
										</tr>
									</tbody>
								</table>

							</div>
							<div class="modal-footer">



								<button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-default" id="save_data">Save</button>
								<button type="button" onClick="update()" style="display: none"
									class="btn btn-warning" id="update_data">Update</button>


							</div>
						</form>
					</div>
				</div>
			</div>

			<!--MODAL-->


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Verify</th>
											<th>View/Print</th>
											<th>Documents</th>
											<th>Registration ID</th>
											<th>Name</th>
											<th>First Name</th>
											<th>Middle Name</th>
											<th>Last Name</th>
											<th>Category</th>
											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Date Of Birth</th>
											<th>Program Allocated</th>
											<th>Status</th>
											<th>Physically Disabled</th>
											<th>Gender</th>
											<th>Nationality</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Student> registration_list = Query.getStudentRegistrationList();
											Iterator<Student> iterator = registration_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
												if (current.getSemester() != 1)
													continue;
										%>
										<tr>
											<td><div class="btn-group">
													<%
														if (!current.getVerified()) {
													%>
													<button type="button" class="btn btn-block btn-primary"
														onclick="verify(<%=current.getRegistration_id()%>)">Verify</button>
													<%
														} else {
													%>
													<button type="button" class="btn btn-block btn-success"
														disabled>Verified</button>
													<%
														}
													%>
												</div></td>

											<td><button type="button" class="btn btn-sm btn-success"
													data-toggle="modal" data-target="#myModal"
													onclick="displayProfile(<%=current.getRegistration_id()%>)">
													<i class="glyphicon glyphicon-eye-open"></i>
												</button>
												<button class="btn btn-sm btn-primary"
													onclick="window.open('../templates/registration.jsp?reg_id=<%=current.getRegistration_id()%>','_blank')">
													<i class="fa fa-print"></i>
												</button></td>
											<td><button type="button"
													class="btn btn-block btn-default "
													onclick="showDocuments(<%=current.getRegistration_id()%>)">
													<i class="glyphicon glyphicon-folder-open"></i>
												</button></td>
											<td><%=current.getRegistration_id()%></td>
											<td><%=current.getName()%></td>
											<td><%=current.getFirst_name()%></td>
											<td><%=current.getMiddle_name()%></td>
											<td><%=current.getLast_name()%></td>
											<td><%=current.getCategory()%></td>

											<td><%=current.getState_eligibility()%></td>
											<td><%=current.getMobile()%></td>
											<td><%=current.getEmail()%></td>
											<td><%=current.getDate_of_birth()%></td>
											<td><%=current.getProgram_allocated()%></td>

											<td><%=current.getStatus()%></td>

											<td><%=StringFormatter.booleanToString(current.isPwd())%></td>
											<td><%=current.getGender()%></td>

											<td><%=current.getNationality()%></td>

										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<script>
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "MM-DD-YYYY" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      
      // Hook up the form so we can prevent it from being posted
      var form = document.getElementById("update");
	 
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        var errors = validate(form, constraints);
		
        if (!errors) {
        	save(form.dataset.reg_id);
			
		
			         }
		else{
		showErrors(form, errors || {});	
		alertify.error('Check the form for errors!');
		return false;
		}
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
                  // Find where the error messages will be insert into
        
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it \:D/
        alert("Success!");
      }
    })();
  </script>
	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->

	<script>
  $(function () {
    $("#example1").DataTable({
		"paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "order": [[ 0, "desc" ]]
	});
  });
  
  
  
 
 
  
  
  
  
  
  
  
  
  function displayProfile(registration_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					//console.log(xmlhttp.responseText);
					var data=JSON.parse(xmlhttp.responseText);
					
					document.getElementById("name").innerHTML=data.name;
					document.getElementById("first_name").innerHTML=data.first_name;
					document.getElementById("middle_name").innerHTML=data.middle_name;
					document.getElementById("last_name").innerHTML=data.last_name;
					document.getElementById("category").innerHTML=data.category;
					document.getElementById("state").innerHTML=data.state;
					document.getElementById("mobile").innerHTML=data.phone_number;
					document.getElementById("email").innerHTML=data.email;
					document.getElementById("date_of_birth").innerHTML=data.date_of_birth;
					document.getElementById("program_allocated").innerHTML=data.program_allocated;
					if(data.physically_disabled==true)
					document.getElementById("physically_disabled").innerHTML="Yes";
					else
						document.getElementById("physically_disabled").innerHTML="No";
					
					if(data.hosteller==true)
						document.getElementById("hosteller").innerHTML="Yes";
						else
							document.getElementById("hosteller").innerHTML="No";
					
					document.getElementById("hostel_address").innerHTML=data.hostel;
					document.getElementById("hostel_room").innerHTML=data.room;
					document.getElementById("semester").innerHTML=data.semester;
					document.getElementById("gender").innerHTML=data.gender;
					document.getElementById("guardian_name").innerHTML=data.guardian_name;
					document.getElementById("guardian_contact").innerHTML=data.guardian_contact;
					document.getElementById("guardian_email").innerHTML=data.guardian_email;
					document.getElementById("guardian_address").innerHTML=data.guardian_address;
					document.getElementById("father_name").innerHTML=data.father_name;
					document.getElementById("father_contact").innerHTML=data.father_contact;
					document.getElementById("mother_name").innerHTML=data.mother_name;
					document.getElementById("mother_contact").innerHTML=data.mother_contact;
					document.getElementById("permanent_address").innerHTML=data.permanent_address;
					document.getElementById("local_address").innerHTML=data.local_address;
					document.getElementById("nationality").innerHTML=data.nationality;
					//document.getElementById("reported").innerHTML=data.reported;
					
					document.getElementById("registration_id").innerHTML=data.registration_id;

					
					//updated data
					
					document.getElementById("update_first_name").value=data.update_first_name;
					document.getElementById("update_middle_name").value=data.update_middle_name;
					document.getElementById("update_last_name").value=data.update_last_name;
					document.getElementById("update_mobile").value=data.update_phone_number;
					document.getElementById("update_email").value=data.update_email;
					if(data.update_hosteller==true)	{
						document.getElementById("update_hosteller_yes").checked=true;
						document.getElementById("update_hosteller_no").checked=false;
					}
					else	{
						document.getElementById("update_hosteller_yes").checked=false;
						document.getElementById("update_hosteller_no").checked=true;
					}
					
					document.getElementById("update_hostel_address").value=data.update_hostel;
					document.getElementById("update_hostel_room").value=data.update_room;
					document.getElementById("update_guardian_name").value=data.update_guardian_name;
					document.getElementById("update_guardian_contact").value=data.update_guardian_contact;
					document.getElementById("update_guardian_email").value=data.update_guardian_email;
					document.getElementById("update_guardian_address").value=data.update_guardian_address;
					document.getElementById("update_father_name").value=data.update_father_name;
					document.getElementById("update_father_contact").value=data.update_father_contact;
					document.getElementById("update_mother_name").value=data.update_mother_name;
					document.getElementById("update_mother_contact").value=data.update_mother_contact;
					document.getElementById("update_permanent_address").value=data.update_permanent_address;
					document.getElementById("update_local_address").value=data.update_local_address;
					 document.getElementById("update").dataset.reg_id=data.registration_id;
					 
					 document.getElementById("update_data").setAttribute("onclick","javascript:return update("+data.registration_id+");");
					
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
		xmlhttp.open("POST","../RetrieveRegistrationStudentData",true);
		
				
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("registration_id="+registration_id);
		}
		return false;
  }

  
 function save(registration_id){
	 var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){
			 var formData=new FormData();

				formData.append("update_first_name",document.getElementById("update_first_name").value);	
				formData.append("update_middle_name",document.getElementById("update_middle_name").value);
		formData.append("update_last_name",document.getElementById("update_last_name").value);
		formData.append("update_mobile",document.getElementById("update_mobile").value);
		
		if(document.getElementById("update_hosteller_yes").checked==true)
			formData.append("update_hosteller",true);
		if(document.getElementById("update_hosteller_no").checked==true)
				formData.append("update_hosteller",false);
	
		formData.append("update_email",document.getElementById("update_email").value);
		
				formData.append("update_hostel_address",document.getElementById("update_hostel_address").value);
				formData.append("update_hostel_room",document.getElementById("update_hostel_room").value);
				formData.append("update_guardian_name",document.getElementById("update_guardian_name").value);
				formData.append("update_guardian_contact",document.getElementById("update_guardian_contact").value);
				formData.append("update_guardian_email",document.getElementById("update_guardian_email").value);
				formData.append("update_guardian_address",document.getElementById("update_guardian_address").value);
				formData.append("update_father_name",document.getElementById("update_father_name").value);
				formData.append("update_father_contact",document.getElementById("update_father_contact").value);
				formData.append("update_mother_name",document.getElementById("update_mother_name").value);
				formData.append("update_mother_contact",document.getElementById("update_mother_contact").value);
				formData.append("update_permanent_address",document.getElementById("update_permanent_address").value);
				formData.append("update_local_address",document.getElementById("update_local_address").value);
				formData.append("registration_id",registration_id);
		
		
		 document.getElementById('update_data').style.display = 'inline';
		 document.getElementById('save_data').style.display = 'none';
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				var data=JSON.parse(xmlhttp.responseText);
	 if(data.success)	alertify.success('Changes have been saved!');
	 else alertify.error(data.errors);
						
					}
					if(xmlhttp.status == 404)
						alert("Could not connect to server");
				}
			
			xmlhttp.open("POST","../UpdateRegistrationStudentDataMultipart",true);
			
	
				
				xmlhttp.send(formData);
			}
			return false;
	
					
					
			
	 
 }

  

function showDocuments(reg_id){
	  window.location.href="documents.jsp?reg_id="+reg_id;
  }

 
 
 function update(registration_id){
	 var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		
		
		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					alertify.confirm("Data updation successful","Click to load changes",function(){window.location.reload();},function(){window.location.reload();});
				
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
				
			}
			xmlhttp.open("POST","../ApplyUpdateRegistrationStudentData",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("registration_id="+registration_id);
		}
		return false;	
	 
 }
 function verify(reg_id){
		
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
				//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		
		if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				
						alertify.confirm("Verification successful!","Student Data has been verified",function(){window.location.href="registration.jsp";},function(){window.location.href="registration.jsp";});
		        	
					
				   
				}
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../VerifyStudent",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("reg_id="+reg_id);
		}
	   }
 
 
 $('#myModal').on('shown.bs.modal', function() {
	 $(".form-group has-error").removeClass("has-error");
	 $(".form-group has-success").removeClass("has-success");
	 $(".messages").html("");
	})
	
	function showDocuments(reg_id){
	  window.location.href="documents.jsp?reg_id="+reg_id;
  }
</script>

</body>
</html>
