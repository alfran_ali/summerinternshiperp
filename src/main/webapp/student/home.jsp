<!DOCTYPE html>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="settings.database.PostgreSQLConnection"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">

<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/select2/select2.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
  <![endif]-->




<script>
	function getPrivacy(html, tag) {

		document.getElementsByClassName("margin dropdown-toggle")[0].innerHTML = html
				+ '<span class="caret"></span><span class="sr-only">Toggle Dropdown</span>';
		document.getElementById("privacy_val").value = tag;
	}

	function autoSuggest(str) {

		var xmlhttp;
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			// Internet Explorer Browsers
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}

		if (xmlhttp) {
			xmlhttp.onreadystatechange = function() {

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

					a = JSON.parse(xmlhttp.responseText);
					document
							.getElementsByClassName("form-control select2 select2-hidden-accessible")[0].innerHTML = "";

					for (var i = 0; i < a.length; i++) {
						document
								.getElementsByClassName("form-control select2 select2-hidden-accessible")[0].innerHTML += "<option>"
								+ a[i].name + "</option>";

					}
				}
				if (xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST", "../AutoSuggest?string=" + str, true);
			xmlhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			xmlhttp.send();
		}

	}

	function redirectProfile() {
		var user = [];
		var json = [];
		var a = [];
		json = JSON.stringify($("#user_list").select2('data')[0]);
		user = JSON.parse(json);
		window.location.href = "profile.jsp?username=" + user.id;

		return false;
	}
</script>
<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Home</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>
			</section>
			<%
				Student student = Query.getStudentProfile(Long.parseLong(session
						.getAttribute("erpId").toString()));
			%>
			
			<span id="post_template" style="display: none">
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<img class="img-circle post_author_pic" src=""
								alt="User Image" height="128px" width="128px"> <span class="username"> <a
								href="#"> <span class="post_author"> </span>
							</a>
							</span> <span class="description"> <span class="post_privacy">Shared
									publicly </span> <span class="post_time">- 7:30 PM Today</span>
							</span>
						</div>
					</div>

					<div class="box-body">
						<!-- post text -->
						<span class="post_text">
							<p>Far far away, behind the word mountains, far from the
								countries Vokalia and Consonantia, there live the blind texts.
								Separated they live in Bookmarksgrove right at</p>

							<p>the coast of the Semantics, a large language ocean. A
								small river named Duden flows by their place and supplies it
								with the necessary regelialia. It is a paradisematic country, in
								which roasted parts of sentences fly into your mouth.</p>
						</span>
						<button type="button" class="btn btn-default btn-xs">
							<i class="fa fa-share"></i> Share
						</button>
						<span class="like_post">
							<i class="fa fa-thumbs-o-up"><button type="button" class="btn btn-default btn-xs likepost"></i><span class="like"> Like</span>
						</button>
						</span>
						<span class="pull-right text-muted"><span
							class="post_likes_count">45 </span> likes - <span
							class="post_comment_count">2</span> comments </span>
					</div>
					<!-- /.box-body -->


					<div class="box-footer box-comments "
						style="background-color: lightgray;"
						style="overflow-y: scroll; height: 250px;">
						<span class="comment_box"></span>
					</div>
					<div class="box-footer">
							<img class="img-responsive img-circle img-sm"
								src="<%=postgreSQLDatabase.authentication.Query.getProfilePicture(Long.parseLong(session.getAttribute("erpId").toString())) %>" alt="Alt Text">
							<!-- .img-push is used to add margin to elements next to floating images -->
							<div class="input-group margin">
								<input type="text" class="form-control col-xs-3 add-comment-text"
									placeholder="Type a comment" onfocus="preventRefresh(true)" onblur="preventRefresh(false)"> <span
									class="input-group-btn">
									<button type="button" class="btn btn-info btn-flat add-comment"
										>Send</button>
								</span>
							</div>
						
					</div>
				</div>

			</span> 
			<span id="comment_template" style="display: none">
				<div class="box-comment">

					<div class="box-comment">
						<!-- User image -->
						<img class="img-circle img-sm comment_author_pic" src=""
							alt="User Image">




						<div class="comment-text">
							<span class="username"><span class="comment_author"></span>
								<span class="text-muted pull-right"> 
								 
								  <span class="like_comment like">
							<span class="comment_like like"><button class="btn btn-default btn-xs likecomment">Like</button></span>
						</span>
						<span class="pull-right text-muted"><span
							class="comment_likes_count">45 </span>
					
								  
               
                  <span class="comment_time"></span>
									</span>
							</span> </span>
							<!-- /.username -->
							<span class="comment_data"></span>
						</div>
					</div>
				</div>

			</span>
			<!-- Main content -->

			<!-- Main content -->
			<section class="content">

				<div class="row">
					<div class="col-md-3">

						<!-- Profile Image -->
						<div class="box box-primary">
							<div class="box-body box-profile">
								<img class="profile-user-img img-responsive img-circle"
									src=<%=postgreSQLDatabase.authentication.Query.getProfilePicture(Long.parseLong(session.getAttribute("erpId").toString())) %>
									alt="User profile picture">

								<h3 class="profile-username text-center"><%=student.getName()%></h3>

								<p class="text-muted text-center"><%=student.getStudent_id()%></p>
								<ul class="list-group list-group-unbordered">
									<li class="list-group-item"><b>Followers</b> <a href=""
										class="pull-right">1,322</a></li>

									<li class="list-group-item"><b>Following</b> <a href=""
										class="pull-right">543</a></li>

									<li class="list-group-item"><b>Friends</b> <a href=""
										class="pull-right">13,287</a></li>
								</ul>

							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->

						<!-- About Me Box -->
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">About Me</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<strong><i class="fa fa-book margin-r-5"></i> Batch</strong>

								<p class="text-muted">
									<%=student.getEmail()%>
								</p>

								<hr>

								<strong><i class="fa fa-file-text-o margin-r-5"></i> ID</strong>

								<p class="text-muted"><%=student.getStudent_id()%></p>

								<hr>

								<strong><i class="fa fa-map-marker margin-r-5"></i>
									Address</strong>

								<p><%=student.getPermanent_address()%></p>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->

					<div class="col-md-7">

						<div class="box box-widget">
							<div class="box-header with-border">
								<div class="user-block">
									<img class="img-circle" src=<%=postgreSQLDatabase.authentication.Query.getProfilePicture(Long.parseLong(session.getAttribute("erpId").toString())) %>
										alt="User Image" height="128px"h="128px"> <span class="username"><a
										href="#"><span class="account_holder"><%=student.getName()%></span></a></span>

								</div>
							</div>
							<div class="box-body">
								<textarea class="form-control"
									style="border: none !important; height: 100px !important; resize: none !important;"
									id="exampleInputEmail1" placeholder="Your Status..."></textarea>
							</div>
							<div class="box-footer">
								<div class="btn-group pull-right">
									<button class="margin dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-globe"></i><b> Publicly</b> <span
											class="caret"></span><span class="sr-only">Toggle
											Dropdown</span>
									</button>

									<ul class="dropdown-menu" role="menu">
										<li><a onclick="getPrivacy(this.innerHTML,'0')"
											style="cursor: pointer;"><i class="fa fa-globe"></i><b>
													Publicly</b></a></li>
										<li class="divider"></li>

										<li><a onclick="getPrivacy(this.innerHTML,'1')"
											style="cursor: pointer;"><i class="fa fa-users"></i><b>
													Friends</b></a></li>

										<li class="divider"></li>

										<li><a onclick="getPrivacy(this.innerHTML,'2')"
											style="cursor: pointer;"><i class="fa fa-expeditedssl"></i><b>
													Only Me</b></a></li>
									</ul>
									<input type='hidden' id="privacy_val" value="0" />
									<button class="margin" onclick="addNewPost()">Post</button>
								</div>
							</div>
						</div>
						<!-- NEWSFEED -->
						<span id="newsfeed"> </span>
						<!-- NEWSFEED -->
					</div>
				</div>
			</section>

			<!-- /.content -->
			<%@ include file="chats.jsp"%>
			<script src="../dist/js/chats.js"></script>
		</div>

		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../plugins/select2/select2.full.min.js"></script>
	<script>
		var pageSize = 20;
		$("#user_list").select2({
			ajax : {
				url : "../AutoSuggest",
				dataType : 'json',
				delay : 250,
				type : "POST",
				data : function(params) {
					return {
						string : params.term, // search term
						page : params.page
					};
				},
				processResults : function(data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = params.page || 1;

					// !IMPORTANT! your every item in data.items has to have an .id property - this is the actual value that Select2 uses
					// Luckily the source data.items already have one
					return {
						results : data.items,
						pagination : {
							more : (params.page * 30) < data.total_count
						}
					};
				},
				cache : true
			},
			escapeMarkup : function(markup) {
				return markup; // let our custom formatter work
			},
			minimumInputLength : 1,
			templateResult : function(repo) {
				if (repo.loading)
					return repo.text;
				return repo.full_name;
			},
			templateSelection : function(repo) {
				return repo.full_name || repo.text;
			}
		});
		function getData() {
			var users = [];
			for (i = 0; i < $("#user_list").select2('data').length; i++) {
				//  	alert(JSON.stringify($("#user_list").select2('data')[i]));
				users.push()
			}
			return false;
		}
	</script>
	<script>
		function addNewPost() {
			var status = document.getElementById("exampleInputEmail1").value;
			var privacy = document.getElementById("privacy_val").value;
			if(status=="") return false;
			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			//var xmlhttp=new XMLHttpRequest();

			if (xmlhttp) {
		

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("exampleInputEmail1").value="";
						alertify.success("Status updated successfully!");
					}
					if (xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp.open("POST", "../RetrievePosts?action=addPost", true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				xmlhttp.send("privacy=" + privacy + "&text=" + status);
			}
			return false;
		}

		
		function setCommentLike(comment_id,like_button){
			 
			  var a=like_button.innerHTML;
			  if(a=="Like"){like_button.innerHTML="Unlike";
			  }
			  if(a=="Unlike"){like_button.innerHTML="Like";
			  
			  }
			  var xmlhttp;
			  try{
				  xmlhttp = new XMLHttpRequest ();
			  }
			  catch(e){
				  
				  try{
					  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
				  }
				  catch(e){
					  try{
						  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					  }
					  catch(e){
						  alert("Your browser is unsupported");
					  }
				  }
			  }
			  if(xmlhttp){
				    xmlhttp.onreadystatechange=function() {
				    	
				        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				        	
						}
				        
				        if(xmlhttp.status == 404)
							alert("Could not connect to server");
						}
				    //alert("hello");
				    if(a=="Like"){
				    xmlhttp.open("POST","../RetrievePosts?action=like_comment",true);
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				    xmlhttp.send("comment_id="+comment_id);
				    }
				    else if(a=="Unlike"){
				    	xmlhttp.open("POST","../RetrievePosts?action=unlike_comment",true);
						xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					    xmlhttp.send("comment_id="+comment_id);
				    }
				    }

		  }

		
		
		
		
		function setLike(post_id,send_button) {
			//var comment = send_button.parentNode.parentNode.getElementsByClassName("form-control col-xs-3 add-comment-text")[0].value;
			//if(comment=="") return false;
		
			if (send_button.innerHTML=="Like")
				send_button.innerHTML="Unlike";
			else
				send_button.innerHTML="Like";
			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			//var xmlhttp=new XMLHttpRequest();

			if (xmlhttp) {

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					}
					if (xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp
						.open("POST", "../RetrievePosts?action=setLike",
								true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				xmlhttp.send("post_id="+post_id);
				
			}
return false;
		}
		
		
		function addNewComment(post_id,send_button) {
			var comment = send_button.parentNode.parentNode.getElementsByClassName("form-control col-xs-3 add-comment-text")[0].value;
			if(comment=="") return false;
			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			//var xmlhttp=new XMLHttpRequest();

			if (xmlhttp) {

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					}
					if (xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp
						.open("POST", "../RetrievePosts?action=addComment",
								true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				xmlhttp.send("post_id="+post_id + "&text=" + comment);
				
			}
			send_button.parentNode.parentNode.getElementsByClassName("form-control col-xs-3 add-comment-text")[0].value="";
			return false;

		}

		function refreshTimeline() {
			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();

			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			//var xmlhttp=new XMLHttpRequest();

			if (xmlhttp) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("newsfeed").innerHTML="";
						//alert(xmlhttp.responseText);
						var data_array = JSON.parse(xmlhttp.responseText);
						for(var j=0;j<data_array.length;j++)
						{
							var data=data_array[j];
							var comments_array = [];
						var post_template = document
								.getElementById("post_template");
						post_template.getElementsByClassName("post_author")[0].innerHTML = data.post_author;
						post_template.getElementsByClassName("img-circle post_author_pic")[0].setAttribute("src","../ftp/profile_picture/image_"+data.post_author_username+".png");
						post_template.getElementsByClassName("post_privacy")[0].innerHTML = data.post_privacy;
						post_template.getElementsByClassName("post_time")[0].innerHTML = data.post_time;
						post_template.getElementsByClassName("post_text")[0].innerHTML = data.post_text;
						post_template.getElementsByClassName("post_comment_count")[0].innerHTML = data.comments_count;
						post_template.getElementsByClassName("post_likes_count")[0].innerHTML = data.likes_count;
						post_template.getElementsByClassName("btn btn-info btn-flat add-comment")[0].setAttribute("onclick","addNewComment("+data.post_id+",this)");
						post_template.getElementsByClassName("form-control col-xs-3 add-comment-text")[0].setAttribute("onchange","addNewComment("+data.post_id+",this)");
						if(data.selfLike==true){
							 post_template.getElementsByClassName("btn btn-default btn-xs likepost")[0].innerHTML="Unlike";
						}
						else{
							post_template.getElementsByClassName("btn btn-default btn-xs likepost")[0].innerHTML="Like";
							 
						}
						post_template.getElementsByClassName("btn btn-default btn-xs likepost")[0].setAttribute("onclick","setLike("+data.post_id+",this)");
				
						 
							 
						  
						post_template.getElementsByClassName("comment_box")[0].innerHTML = "";
						var json = JSON.parse(data.post_comments);
						for (i = 0; i < json.length; i++) {
							var comment_div = document.getElementById("comment_template");
							comment_div.getElementsByClassName("comment_author")[0].innerHTML = json[i].comment_author;
							comment_div.getElementsByClassName("img-circle img-sm comment_author_pic")[0].setAttribute("src","../ftp/profile_picture/image_"+json[i].comment_author_username+".png");
							if(json[i].selfCommentLike==true){
								comment_div.getElementsByClassName("btn btn-default btn-xs likecomment")[0].innerHTML="Unlike";
							}
							else{
								comment_div.getElementsByClassName("btn btn-default btn-xs likecomment")[0].innerHTML="Like";
								 
							}
							//alert(comment_div.getElementsByClassName("like_comment like")[0]);
							//alert(comment_div.getElementsByClassName("like_comment").innerHTML);
							comment_div.getElementsByClassName("btn btn-default btn-xs likecomment")[0].setAttribute("onclick","setCommentLike("+json[i].comment_id+",this)");
							comment_div.getElementsByClassName("comment_likes_count")[0].innerHTML=json[i].comments_likes_count;
							comment_div.getElementsByClassName("comment_data")[0].innerHTML = json[i].comment_text;
							comment_div.getElementsByClassName("comment_time")[0].innerHTML = json[i].comment_time;
							post_template.getElementsByClassName("comment_box")[0].innerHTML += comment_div.innerHTML;
						}

						document.getElementById("newsfeed").innerHTML += post_template.innerHTML;
						}
					}
					if (xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp.open("POST", "../RetrievePosts?action=retrievePosts",
						true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				xmlhttp.send();
			}
		}
		
		var refresh=true;
		function preventRefresh(active){
			
			refresh=!active;
		}
		refreshTimeline();
		window.setInterval(function (){
		if(refresh)refreshTimeline();
		},5000);
		//refreshTimeline();
		
	</script>

</body>
</html>