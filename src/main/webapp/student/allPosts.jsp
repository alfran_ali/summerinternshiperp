<%@page import="java.util.Date"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="forum.ForumThread"%>
<%@page import="forum.Post"%>
<%@page import="forum.Comment"%>
<%@page import="forum.Category"%>
<%@page import="postgreSQLDatabase.forum.Query"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IIIT KOTA | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../plugins/ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<input type="hidden" id="thread_id" value="<%=Long.parseLong(request.getParameter("thread_id")) %>"/>
<input type="hidden" id="erp_id" value="<%=Long.parseLong(session.getAttribute("erpId").toString()) %>"/>
<span class="new_post" style="display:none">

</span>
<span id="like_post" style="display:none">
<button type="button" class="btn btn-default btn-xs likePost"></button>
						</span>
<span id="post_delete" style="display:none">

                <button type="button" class="btn btn-box-tool editPost" data-toggle="tooltip" title="Edit post">
                  <i class="fa fa-pencil-square-o"></i></button>
                
                <button type="button" class="btn btn-box-tool deletePost"><i class="fa fa-times"></i></button>
              
</span>

<span id="comment_delete" style="display:none">
  <button type="button" class="btn btn-box-tool editComment" data-toggle="tooltip" title="Edit comment">
  
                  <i class="fa fa-pencil-square-o"></i></button>
                
                <button type="button" class="btn btn-box-tool deleteComment">
                <i class="fa fa-times"></i></button>
              
</span>

<span id="comment" style="display:none">
<div class="box-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">
               
                
            

                <div class="comment-text">
                    <span class="username"><span class="comment_username"></span> 
                        
                        <span class="text-muted pull-right">
                        <span class="comment_timestamp">  
                          </span>        
                        
                        
              <span class="comment_buttons"></span>
                        </span>
                        </span> 
                      <!-- /.username -->
                  <span class="comment_data"></span>
                </div>
 </div>
</span>
<span id="post" style="display:none">
    	<div class="row">
    		<div class="col-md-8">
          <div class="box box-widget">
            
            <div class="box-header with-border">
            
              <div class="user-block">
                
                <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                <span class="username">
                <span class="post_username"><a href="#"></a></span></span>
               <span class="description"><span class="time"></span></span> 
              </div>
              
              
              <div class="box-tools">
              <span class="post_buttons"></span> 
              </div>
              
            </div>
           
            <div class="box-body">
              <!-- post text -->
              <p><span class="post_name"></span></p>
              <span class="post_like"></span>
              <span class="pull-right">
              <span class="likes_count"></span>
              <span class="fa fa-thumbs-o-up"></span>
			  </span>
            </div>
            
            
            <!-- /.box-body -->
            <div class="box-footer box-comments" style="background-color:lightgray;">
           
           <span class="comments"></span>
            
            </div>
            <div class="box-footer">
              
                <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                
                <div class="img-push">
                  <input type="text" class="form-control input-sm addComment" 
                  placeholder="Press enter to post comment" name="comment" >
                </div>
              
            </div>
            </div>
    </div>
    </div>
    
    </span>
    <!--  -->
<div class="wrapper">
<%@ include file="header.jsp" %>
 <!-- Left side column. contains the logo and sidebar -->
 <%@ include file="main-sidebar.jsp" %>
 
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <%Long thread_id=Long.parseLong(request.getParameter("thread_id"));
      ForumThread thread=Query.getSingleThread(thread_id);
      Long category_id=thread.getParent_category_id();
      String thread_name=thread.getThread_name();
      Date time=thread.getTimestamp();
      Long author_id=thread.getAuthor_id();
      String author_name=Query.getAuthorName(author_id);
      String category_name=Query.getCategoryName(category_id); %>
        FORUMS<br />
        <div class="col-md-8" style="margin-left:-1%;">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i>
        <br/>
        <label></label>
        </span>
        <div class="info-box-content">
        <span class="info-box-text"><%=category_name %></span>
        <span class="info-box-number"><%=thread_name %></span>
            
      <%
      Long thread_subscriber_id=Long.parseLong(session.getAttribute("erpId").toString());
      boolean thread_status=Query.getThreadSubscriberStatus(thread_id, thread_subscriber_id);
      if(thread_status==false){ %>
      <div class="btn-group pull-right">
														
			<button id="subscribeThread" class="btn btn-primary btn-xs btn-info" >Subscribe</button> 
	</div>
	<%}else if(thread_status==true){ %>
	<div class="btn-group pull-right">
														
			<button id="subscribeThread" class="btn btn-danger btn-xs btn-info" >Unsubscribe</button> 
	</div>
	<%} %>
	  </div>
         </div></div>
      </h1>
	
	
	
      <ol class="breadcrumb">
        <li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>
        
        
      </ol>
    </section>
      <section class="content">
    <span id="feed"></span>
    
   <!--  <form action="../AddNewPost?thread_id=<%= thread_id %>" method="post">--> 
										
										<div class="row ">
    <div class="col-md-8">
                               <div class="modal-content">
											<div class="modal-header">
												<div class="timeline-footer pull-right">
												</div>
												
												<h4 class="modal-title">
												
												</h4>
											</div>
											
											<div class="modal-body">
												<div class="form-group">
													<textarea class="form-control" style="border-top:none !important;border-right:none !important;border-left:none !important;height:100px !important;resize:none !important;"id="post_data" placeholder="Your Post..." ></textarea>								
													<br />
													
													<div class="btn-group pull-right">
														<button class="btn btn-primary btn-xs btn-info" onclick="addPost(<%=thread_id%>)">Send Post </button>
													</div>
                								</div>
											</div>
											
											<div class="modal-body pull-right">
											</div>
										</div>
										</div>
										</div>
										<!-- </form> -->
										
     
    
         
    	

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <%@ include file="footer.jsp" %>
  <!-- Control Sidebar -->
  <%@ include file="control-sidebar.jsp" %>
 </div>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
var torefresh=true;
  $.widget.bridge('uibutton', $.ui.button);
  
  function addPost(thread_id){
	  if(document.getElementById("post_data").value==""){return;}
	  torefresh=false;
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	//alert(document.getElementById("post_data").value);
		        	//alert(thread_id);
		        	var a=document.getElementById("subscribeThread").innerHTML;
		      	  //alert(a);
		      	  if(a=="Subscribe"){document.getElementById("subscribeThread").innerHTML="Unsubscribe";
		      	  document.getElementById("subscribeThread").setAttribute("class","btn btn-danger btn-xs btn-info");
		      	  }
		      	  if(a=="Unsubscribe"){document.getElementById("subscribeThread").innerHTML="Subscribe";
		      	  document.getElementById("subscribeThread").setAttribute("class","btn btn-primary btn-xs btn-info");
		      	  
		      	  }
		        	update(document.getElementById("thread_id").value);	
		        	torefresh=true;
		        	document.getElementById("post_data").value="";
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../AddNewPost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("post_name="+document.getElementById("post_data").value+"&thread_id="+thread_id);
		    }

  }
  
  function update(thread_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	
		        	createPost(xmlhttp.responseText);
		        	//location.reload();	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../RefreshThread",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("thread_id="+thread_id);
		    }

  }
  
  function createPost(object){
	  document.getElementById("feed").innerHTML="";
	  var json=JSON.parse(object);
	 //alert(JSON.stringify(json));
	 //System.out.println(JSON.stringify(json));
	  document.getElementById("subscribeThread").setAttribute("onclick","javascript:subscribeThread("+document.getElementById("thread_id").value+")");
	  
	  for(i=0;i<json.length;i++){
		  //alert(JSON.stringify(json[i]));
		  var post_template=document.getElementById("post");//alert(post_template);
		  post_template.getElementsByClassName("form-control input-sm addComment")[0].setAttribute("onkeypress","javascript:addComment(event,this,"+json[i]["post_id"]+")");
		 // alert(json[i]["selfLike"]);
		 post_template.getElementsByClassName("post_like")[0].innerHTML="";
		 post_template.getElementsByClassName("likes_count")[0].innerHTML="";
		  if(json[i]["selfLike"]==true){
			  var like_template=document.getElementById("like_post");
			  like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].innerHTML="Unlike";
			  like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].setAttribute("onclick","javascript:likePost("+json[i]["post_id"]+",this)");
			  post_template.getElementsByClassName("post_like")[0].innerHTML+=like_template.innerHTML;  
		  }
		  else{
			  var like_template=document.getElementById("like_post");
			  like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].innerHTML="Like";
			  like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].setAttribute("onclick","javascript:likePost("+json[i]["post_id"]+",this)");
			  post_template.getElementsByClassName("post_like")[0].innerHTML+=like_template.innerHTML;
		  }
		 // post_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].setAttribute("onclick","javascript:likePost("+json[i]["post_id"]+")");
		 if(json[i]["count"]!=0){
		 post_template.getElementsByClassName("likes_count")[0].innerHTML=json[i]["count"];
		 }
		  post_template.getElementsByClassName("post_name")[0].innerHTML=json[i]["post_name"];
		  post_template.getElementsByClassName("post_username")[0].innerHTML=json[i]["post_author_name"];
		  post_template.getElementsByClassName("time")[0].innerHTML=json[i]["timestamp"];
		  post_template.getElementsByClassName("post_buttons")[0].innerHTML="";
		  if(json[i]["post_author"]==true){
			  var button_template=document.getElementById("post_delete");
			  button_template.getElementsByClassName("btn btn-box-tool deletePost")[0].setAttribute("onclick","javascript:deletePost("+json[i]["post_id"]+")");
			  button_template.getElementsByClassName("btn btn-box-tool editPost")[0].setAttribute("onclick","javascript:editPost(this,"+json[i]["post_id"]+")");
			  post_template.getElementsByClassName("post_buttons")[0].innerHTML+=button_template.innerHTML;
			  		  
		  }
		  
		  
		  var comment_json=json[i]["comments"];
		  post_template.getElementsByClassName("comments")[0].innerHTML="";
		  
		  for(j=0;j<comment_json.length;j++){
			  var comment_template=document.getElementById("comment");
			  
			  comment_template.getElementsByClassName("comment_username")[0].innerHTML=comment_json[j]["comment_author_name"];
			  comment_template.getElementsByClassName("comment_data")[0].innerHTML=comment_json[j]["data"];
			  comment_template.getElementsByClassName("comment_timestamp")[0].innerHTML=comment_json[j]["timestamp"];
			  comment_template.getElementsByClassName("comment_buttons")[0].innerHTML="";
			  if(comment_json[j]["comment_author"]==true){
				  var button1_template=document.getElementById("comment_delete");
				  button1_template.getElementsByClassName("btn btn-box-tool deleteComment")[0].setAttribute("onclick","javascript:deleteComment("+comment_json[j]["comment_id"]+")");
				  button1_template.getElementsByClassName("btn btn-box-tool editComment")[0].setAttribute("onclick","javascript:editComment(this,"+comment_json[j]["comment_id"]+")");
				  comment_template.getElementsByClassName("comment_buttons")[0].innerHTML+=button1_template.innerHTML;
				  		  
			  }
			  post_template.getElementsByClassName("comments")[0].innerHTML+=comment_template.innerHTML;
		  }
		  document.getElementById("feed").innerHTML+=post_template.innerHTML;
	  }
	  
	 // var new_post_template=document.getElementsByClassName("new_post")[0];
	  //new_post_template.getElementsByClassName("btn btn-primary btn-xs btn-info addPost")[0].setAttribute("onclick","javascript:addPost(this,"+document.getElementById("thread_id")+")");
	  
	//  document.getElementById("add_posts").innerHTML+=new_post_template.innerHTML;
  }
  
  function editPost(edit_button,post_id){
	 
	  torefresh=false;
		        	var span=edit_button.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("post_name")[0];
		        	//alert(span);
		        	var data=span.innerHTML;
		        	//alert(data);
		        	span.innerHTML="<textarea rows=4 cols=100 type='text' class='edit_post' onblur='updatePost(this.value,"+post_id+")'>"+data+"</textarea>";
		        	
		        	var updated_data=span.getElementsByClassName("edit_post")[0].innerHTML;
		        	//updatePost(updated_data,post_id);
		        	//span.innerHTML=updated_data;
  }
  
  function updatePost(updated_data,post_id){
	  if(updated_data==""){updated_data=data;}
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	//alert(updated_data);
		        	//var span=edit_button.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("post_name")[0];
		        	//span.innerHTML=updated_data;
		        	//alert(document.getElementById("thread_id").innerHTML);
		        	update(document.getElementById("thread_id").value);	
		        	torefresh=true;
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../EditPost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("updated_data="+updated_data+"&post_id="+post_id);
		    }

  }
  
  function editComment(edit_button,comment_id){
	  torefresh=false;
		        	var span=edit_button.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("comment_data")[0];
		        	//alert(span);
		        	var data=span.innerHTML;
		        	
		        	span.innerHTML="<input type='text' class='edit_comment' onblur='updateComment(this.value,\""+data+"\",\""+comment_id+"\")' value='"+data+"' />";
		        	var updated_data=span.getElementsByClassName("edit_comment")[0].innerHTML;
		        	//updateComment(updated_data,comment_id,author_id);
		        	//span.innerHTML=updated_data;
		        	
  }
  
  function updateComment(updated_data,data,comment_id){
	  //alert(data);
	  if(updated_data==""){updated_data=data;}
	  
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	
		        	
		        	update(document.getElementById("thread_id").value);	
		        	torefresh=true;
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../EditComment",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("updated_data="+updated_data+"&comment_id="+comment_id);
		    }
	  
  }
  
  function subscribeThread(thread_id){
	  var a=document.getElementById("subscribeThread").innerHTML;
	  //alert(a);
	  if(a=="Subscribe"){document.getElementById("subscribeThread").innerHTML="Unsubscribe";
	  document.getElementById("subscribeThread").setAttribute("class","btn btn-danger btn-xs btn-info");
	  }
	  if(a=="Unsubscribe"){document.getElementById("subscribeThread").innerHTML="Subscribe";
	  document.getElementById("subscribeThread").setAttribute("class","btn btn-primary btn-xs btn-info");
	  
	  }
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	update(document.getElementById("thread_id").value);	
		        	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    //alert("hello");
		    if(a=="Subscribe"){
		    xmlhttp.open("POST","../SubscribeThread",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("thread_id="+thread_id);
		    }
		    else if(a=="Unsubscribe"){
		    	xmlhttp.open("POST","../UnsubscribeThread",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("thread_id="+thread_id);
		    }
		    }

  }
  
  function subscribePost(post_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	update(document.getElementById("thread_id").value);	
		        	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    //alert("hello");
		    xmlhttp.open("POST","../SubscribePost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("post_id="+post_id);
		    }

  }
  
  function deleteComment(comment_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	update(document.getElementById("thread_id").value);	
		        	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    //alert("hello");
		    xmlhttp.open("POST","../DeleteComment",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("comment_id="+comment_id);
		    }

  }
  
  function deletePost(post_id){
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	
		        	
		        	update(document.getElementById("thread_id").value);	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../DeletePost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("post_id="+post_id);
		    }

  }
  
  function likePost(post_id,like_button){
	  //alert("hello");
	  var a=like_button.innerHTML;
	 
	  if(a=="Like"){a.innerHTML="Unlike";
	  //like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].setAttribute("class","btn btn-danger btn-xs btn-info");
	  }
	  if(a=="Unlike"){a.innerHTML="Like";
	  //like_template.getElementsByClassName("btn btn-default btn-xs likePost")[0].setAttribute("class","btn btn-primary btn-xs btn-info");
	  
	  }
	  var xmlhttp;
	  try{
		  xmlhttp = new XMLHttpRequest ();
	  }
	  catch(e){
		  
		  try{
			  xmlhttp = new ActiveXObject(" msxml2.XMLHTTP");
		  }
		  catch(e){
			  try{
				  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  }
			  catch(e){
				  alert("Your browser is unsupported");
			  }
		  }
	  }
	  if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		        	update(document.getElementById("thread_id").value);	
		        	
				}
		        
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    //alert("hello");
		    if(a=="Like"){
		    xmlhttp.open("POST","../LikePost",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("post_id="+post_id);
		    }
		    else if(a=="Unlike"){
		    	xmlhttp.open("POST","../UnlikePost",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("post_id="+post_id);
		    }
		    }

  }
  
  function addComment(e,input,post_id){
	  if(input.value=="")
		  {return;}
	  else{
	  torefresh=false;
	  if(e.keyCode==13){
		  //alert(input.value);
		  //alert(post_id);
		  var xmlhttp;
			try{
				xmlhttp = new XMLHttpRequest();
			} catch (e){
				// Internet Explorer Browsers
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try{
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e){
					//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}	
			
			if(xmlhttp){
			    xmlhttp.onreadystatechange=function() {
			    	
			        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			        	
			        	
			        	update(document.getElementById("thread_id").value);	
			        	torefresh=true;
					}
			        
			        if(xmlhttp.status == 404)
						alert("Could not connect to server");
					}
			    xmlhttp.open("POST","../AddNewComment",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("text="+input.value+"&post_id="+post_id);
			    }
	  }
	  }
  }
  update(document.getElementById("thread_id").value);
  
	  //setInterval(function(){ if(torefresh==true)update(document.getElementById("thread_id").value); }, 10000);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script src="../plugins/select2/select2.full.min.js"></script>

</body>
</html>