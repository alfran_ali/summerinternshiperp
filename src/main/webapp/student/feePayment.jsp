<!DOCTYPE html>
<%@page import="postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONArray"%>
<%@page import="postgreSQLDatabase.registration.Query"%>

<%@page import="org.json.JSONObject"%>


<%@ page import="users.Student"%>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ERP IIITK</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<script type="text/javascript">
	var a;
	$(document).ready(function() {
		$("#step-2").hide();
		$("#step-3").hide();
		$("#step-4").hide();
		$("#step-5").hide();
		$("#step-6").hide();
		$("#step-7").hide();
		$("#error").hide();

		$("#step1").click(function() {
			$("#step-1").hide();
			$("#step-2").toggle('slide', 'right', 500);
		});

		$("#step_2").click(function() {
			$("#step-2").hide();
			$("#step-1").toggle("slide", "left", 500);
		});

	});

	function showPayment() {
		a = $('input[name=payment]:checked').val();
		if (a == null) {
			$("#error").show();
		} else {
			$("#step-2").hide();
			$("#" + a).toggle('slide', 'right', 500);
		}
	}

	function back() {
		$("#" + a).hide();
		$("#step-2").toggle('slide', 'right', 500);
	}
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini"
	style="background: url('../image/image.jpg'); background-size: cover;">
	<div class="wrapper">
		<%@ include file="header.jsp"%>

		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<div class="content-wrapper" style="min-height: 901px;">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<%
							
								Long erp_id = Long.parseLong(request.getSession().getAttribute("erpId").toString());
                                 Long reg_id=Query.getRegistrationId(erp_id);
								Student current = Query.getRegistrationStudentData(reg_id);
								//String fee = Query.getFeeBreakup(Integer.parseInt(reg_id));
							%>
							<div id="step-1">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 1</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<label for="exampleInputPassword1">Registration ID
														: <%=reg_id%>
													</label>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Student Name : <%=current.getName()%></label>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Category : <%=current.getCategory()%></label>
												</div>
												
												
												
												<% JSONArray details=  postgreSQLDatabase.feePayment.Query.getFeeJson(reg_id);
								                
												%>
												<div class="form-group">
													<label for="exampleInputPassword1" >Amount : <span id="fee_amount"><%=details.getJSONObject(details.length()-1).getInt("total")%></span></label>
												</div>
												<div class="form-group">
													<label for="exampleInputPassword1">Fee Breakup : </label>
													<table border="1" width="100%"><%
											  		//System.out.println(details);
				       	                            for(int i=0;i<details.length()-1;i++){
						                			JSONObject sub_category= details.getJSONObject(i);
						                			String sub_category_header =sub_category.keys().next(); 
						                			out.print("<tr><td colspan='4' style='padding-left:5%;'><strong>"+sub_category_header.toUpperCase()+"</strong></td></tr>");
						                		JSONArray fields=sub_category.getJSONArray(sub_category_header);
						                		   for(int j=0;j<fields.length();j++){
						                			   JSONObject field= fields.getJSONObject(j);
						                				 String keys=  field.keys().next();
						                				 out.print("<tr><td  colspan='2' style='padding-left:5%;'>"+keys.toUpperCase()+"  "+"</td><td style='padding-left:5%;'>"+ field.getInt(keys)+"</td></tr>");
						                			   }
				       	                            }
							                		  %>
													</table>
												</div>

												<div class="row">
													<div class="col-xs-4 pull-right">
														<input type="button" name="step-1" id="step1" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>
							</div>

							<div id="step-2">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 2</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post" id="payment_method">
												<div class="form-group">
													<label for="exampleInputPassword1">Registration ID
														: <%=reg_id%>
													</label>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Student Name : <%=current.getName()%></label>
												</div>
												<span id="error">
													<div class="form-group">
														<label style="color: red;">Please select any one
															of the payment method</label>
													</div>
												</span>
												<div class="form-group">


													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-3"> Demand Draft
														</label>
													</div>
													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-4"> Challan
														</label>
													</div>
													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-7"> NEFT
														</label>
													</div>

												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" name="step_2" id="step_2"
															value="Prev" class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" name="step-2" id="step2"
															onclick="showPayment()" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-3">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>Demand Draft</h3>
												</div>

												<div class="form-group has-feedback">
												    <label>DD Number</label>
													<input type="text" name="dd" id="dd_no"
														class="form-control" placeholder="DD Number">
												</div>

												<div class="form-group has-feedback">
													<label>Bank</label>
													<select class="form-control" name="bank_list" id="dd_bank">
														<option value="">----Choose Bank----</option>
														<option value="">ICICI</option>
														<option value="">SBI</option>
														<option value="">Bank Of Baroda</option>
														<option value="AllahabadBank">Allahabad Bank</option>
														<option value="AndhraBank">Andhra Bank</option>
														<option value="AxisBank">Axis Bank</option>
														<option value="BankOfIndia">Bank of India</option>
														<option value="BankOfMaharashtra">Bank of
															Maharashtra</option>
														<option value="CanaraBank">Canara Bank</option>
														<option value="IndianBank">Indian Bank</option>
														<option value="UCOBank">UCO Bank</option>
														<option value="YesBankLtd">Yes Bank Ltd</option>
														<option value="KotakBank">Kotak Bank</option>
														<option value="UnionBankOfIndia">Union Bank of
															India</option>
														<option value="UnitedBankOfIndia">United Bank of
															India</option>
													</select>
												</div>



												<div class="form-group has-feedback">
													<label for="exampleInputPassword1">Payable To :
														Director, IIIT Kota</label>
												</div>

												<div class="form-group has-feedback">
													<label for="exampleInputPassword1">Payable At :
														Jaipur</label>
												</div>

												<div class="form-group">
													<div class="input-group">
														<label>Date</label>
														<input type="text" name="dateofDD" id="dd_date"
															class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
													<!-- /.input group -->
												</div>

												<div class="form-group has-feedback">
													<label>Amount</label>
													<input type="text" id="dd_amount" name="amountDD"
														class="form-control" placeholder="Amount">
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" onclick="makePayment('dd')"
															name="submitDD" value="Submit"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-4">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											
												<div class="form-group">
													<h3>Challan</h3>
												</div>





<div class="form-group has-feedback">
												    <label>DD Number/Check Number</label>
													<input type="text" id="challan_no"
														class="form-control" placeholder="Challan Number">
												</div>

												<div class="form-group has-feedback">
													<label>Bank</label>
													<select class="form-control" name="bank_list" id="challan_bank">
														<option value="">----Choose Bank----</option>
														<option value="">ICICI</option>
														<option value="">SBI</option>
														<option value="">Bank Of Baroda</option>
														<option value="AllahabadBank">Allahabad Bank</option>
														<option value="AndhraBank">Andhra Bank</option>
														<option value="AxisBank">Axis Bank</option>
														<option value="BankOfIndia">Bank of India</option>
														<option value="BankOfMaharashtra">Bank of
															Maharashtra</option>
														<option value="CanaraBank">Canara Bank</option>
														<option value="IndianBank">Indian Bank</option>
														<option value="UCOBank">UCO Bank</option>
														<option value="YesBankLtd">Yes Bank Ltd</option>
														<option value="KotakBank">Kotak Bank</option>
														<option value="UnionBankOfIndia">Union Bank of
															India</option>
														<option value="UnitedBankOfIndia">United Bank of
															India</option>
													</select>
												</div>


                                                <div class="form-group">
													<div class="input-group">
														<label>Date</label>
														<input type="text" name="dateofDD" id="challan_date"
															class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
												
												</div>




















                                            
												</div>


												<div class="form-group"></div>
												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" name="submitChallan" value="Submit"
															onclick="makePayment('challan')"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

										
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>


							<div id="step-7">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>NEFT</h3>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Beneficiary
														Account : 120000</label>
												</div>

												<div class="form-group has-feedback">
													<label>IFSC Code</label>
													<input type="text" id="ifsc" name="ifsc"
														class="form-control" placeholder="IFSC Code">
												</div>

												<div class="form-group has-feedback">
													<div class="input-group">
														<label>Transaction ID</label>
														<input type="text" id="tid" name="tid"
															class="form-control" placeholder="Transaction ID">
													</div>
												</div>

												<div class="form-group has-feedback">
													<label>Bank</label>
													<select class="form-control" id="neft_bank" name="bank_list"
														id="neft_bank">
														<option value="">----Choose Bank----</option>
														<option value="ICICI">ICICI</option>
														<option value="SBI">SBI</option>
														<option value="Bank Of Baroda">Bank Of Baroda</option>
														<option value="AllahabadBank">Allahabad Bank</option>
														<option value="AndhraBank">Andhra Bank</option>
														<option value="AxisBank">Axis Bank</option>
														<option value="BankOfIndia">Bank of India</option>
														<option value="BankOfMaharashtra">Bank of
															Maharashtra</option>
														<option value="CanaraBank">Canara Bank</option>
														<option value="IndianBank">Indian Bank</option>
														<option value="UCOBank">UCO Bank</option>
														<option value="YesBankLtd">Yes Bank Ltd</option>
														<option value="KotakBank">Kotak Bank</option>
														<option value="UnionBankOfIndia">Union Bank of
															India</option>
														<option value="UnitedBankOfIndia">United Bank of
															India</option>
													</select>
												</div>


                                                       <div class="form-group">
													<div class="input-group">
														<label>Date</label>
														<input type="text"  id="neft_date"
															class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>


												<div class="form-group has-feedback">
													<label>Amount</label>
													<input type="text" id="neft_amount" name="amountNEFT"
														class="form-control" placeholder="Amount">
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" value="Submit"
															onclick="makePayment('neft')"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>


						</div>
						<div class="col-md-3"></div>

					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
	</div>


	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<script>
		$(function() {
			//Datemask dd/mm/yyyy
			$("#datemask").inputmask("dd/mm/yyyy", {
				"placeholder" : "dd/mm/yyyy"
			});
			//Datemask2 mm/dd/yyyy
			$("#datemask2").inputmask("mm/dd/yyyy", {
				"placeholder" : "mm/dd/yyyy"
			});
			//Money Euro
			$("[data-mask]").inputmask();
		});
	</script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../plugins/knob/jquery.knob.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="../plugins/daterangepicker/daterangepicker.js"></script>
	<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<script src="../dist/js/app.min.js"></script>
	<script src="../dist/js/pages/dashboard.js"></script>
	<script src="../dist/js/demo.js"></script>
	<script>

	function makePayment(method) {
var data="";
		if (method == "dd") {
			amount = document.getElementById('dd_amount').value;
			payment_method = "demand_draft";
			date = document.getElementById('dd_date').value;
			bank = document.getElementById('dd_bank').value;
			ref_no = "dd_no=" + document.getElementById('dd_no').value;
			data="amount=" + amount + "&date=" + date + "&payment_method="
			+ payment_method + "&bank=" + bank + "&" +ref_no;
		}

		if (method == "neft") {
			amount = document.getElementById('neft_amount').value;
			payment_method = "neft";
			date = document.getElementById('neft_date').value;
			ifsc = document.getElementById('ifsc').value;
			bank = document.getElementById('neft_bank').value;
			ref_no  = "tid=" + document.getElementById('tid').value;
			data="amount=" + amount + "&date=" + date + "&payment_method="
			+ payment_method + "&bank=" + bank + "&ifsc=" + ifsc + "&" +ref_no ;
			
		}
		if (method == "challan") {
			date = document.getElementById('challan_date').value;
			bank = document.getElementById('challan_bank').value;
			ref_no  = "challan_no=" + document.getElementById('challan_no').value;
			amount = document.getElementById('fee_amount').innerHTML;
			payment_method = "challan";
			data="amount=" + amount + "&date=" + date + "&payment_method="
			+ payment_method + "&bank=" + bank + "&" +ref_no  ;
			
		}
		var xmlhttp;
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			// Internet Explorer Browsers
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					// Browser doesn't support ajax
					alert("Your browser is unsupported");
				}
			}
		}

		if (xmlhttp) {
			xmlhttp.onreadystatechange = function() {

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var json=JSON.parse(xmlhttp.responseText);
				var ref_no=json.ref_id;
				 alert("Payment Information saved successfully! Please proceed to desk 3 for verification!");
						if (method == "challan")
					{
							window.open("../templates/feeChallan.jsp?ref_no="+ref_no, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes");
							//window.location.href="../templates/feeChallan.jsp?ref_no="+ref_no;
					
					}
					
	              
	             window.location.href="home.jsp";
				}
				if (xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			var uid = 1;
			xmlhttp.open("POST", "../AddPaymentInfo", true);
		
			xmlhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			
			xmlhttp.send(data);

		}
	}
	
	</script>
</body>

</html>