<!DOCTYPE html>
<%@page import="forum.ForumThread"%>
<%@page import="postgreSQLDatabase.forum.Query"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.myModal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.myModal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
				<%String category_name=Query.getCategoryName(Long.parseLong(request.getParameter("category_id"))); %>
					TOPICS OF DISCUSSION <small>In category <%=category_name %></small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>



			<!-- Modal -->

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
							
						</div>
					</div>
				</div>
			</div>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<!-- <h3 class="box-title">Add new post to thread</h3> -->
								
							</div>
							<!-- /.box-header -->
							<%
							ArrayList<ForumThread> thread_list=postgreSQLDatabase.forum.Query.getMultipleThreads(Long.parseLong(request.getParameter("category_id")));
							System.out.println(thread_list);
							if(!thread_list.isEmpty()){System.out.println("hello");
							%>
							<div class="box-body" style="overflow-x: scroll;">
							
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Topic</th>
											<th>Author</th>
											<th>Posted</th>
											
									</thead>
									<tbody>
										<%
                	
                                Iterator<ForumThread> iterator=thread_list.iterator();
                                while(iterator.hasNext()){
                    				ForumThread current=iterator.next();
                    				Long thread_id=current.getThread_id();
                %>
										<tr onClick="window.location.href='allPosts.jsp?thread_id=<%=current.getThread_id()%>';">
										<%String username=Query.getAuthorName(current.getAuthor_id()); %>
											
											<td><%=current.getThread_name() %></td>
											<td><%=username %></td>
											<td><%=current.getTimestamp()%></td>
											
										</tr>
										<%
                }
				%>
									</tbody>
								</table>
								<div class="btn-group pull-right">
													<form action="new-thread.jsp" method="GET">	
													<input type="hidden" name="category" value="<%=request.getParameter("category_id")%>"></input>
													<input type="submit" class="btn btn-primary btn-xs btn-info" value="Add new Thread"></input>
													</form></div>
							</div>
							<%} 
							else{%>
							
							<form action="new-thread.jsp" method="GET">	
													<input type="hidden" name="category" value="<%=request.getParameter("category_id")%>"></input>
													<input type="submit" class="btn btn-primary btn-xs btn-info" value="Add new Thread"></input>
													</form>
							<%} %>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script>
  $(function () {
    $("#example1").DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "order": [[ 0, "desc" ]]
	});
  });
</script>
	

</body>
</html>