<%@page import="utilities.StringFormatter"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%
	Student student;
	if (request.getParameter("reg_id") != null)
		student = Query.getRegistrationStudentData(Long.parseLong(request.getParameter("reg_id")));
	else
		student = new Student();
%>



<html>
<head>
</head>

<style>
@page {
	size: A4;
}

th, td {
	border: 1px solid black;
	border-collapse: collapse;
	width: auto;
	margin-left: auto;
	margin-right: auto;
}

li {
	margin-left: 10%;
}

.tabl1 {
	border: none !important;
	border-collapse: collapse;
	width: 100%;
	font-weight: bold;
}

.tabl3 {
	border: 1px solid black;
	border-collapse: collapse;
	width: 100%;
	font-weight: bold;
}

.tabl {
	margin-left: 13%;
}

.p0 {
	margin-left: 10%;
}

}
h1 {
	display: block;
	font-size: 2em;
	margin-top: 0.67em;
	margin-bottom: 0.1em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	align: center;
}

h2 {
	display: block;
	font-size: 1.2em;
	margin-top: 0.67em;
	margin-bottom: 0.1em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	text-decoration: underline;
}

p {
	font-size: 1em;
}

th, td {
	padding: 5px;
	text-align: center;
}
</style>
<title>REGISTRATION FORM</title>
</head>
<body>


	<p align="center">
		<img alt=""
			src="../image/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>
	<center>
		REGISTRATION FORM ACADEMIC SESSION
		<%=Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()))%>-<%=Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())) + 1%></center>
	</h2>
	</br>


	<table class="tabl1" style="table-layout: fixed">
		<tr>
			<td><strong>Date of Registration</strong></td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getEntry_date())%></td>

			<td><strong>Semester</strong></td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getSemester())%></td>
			<td rowspan="2" style="border:none">Student <br> <br> <br> Photograph</td>
		<tr>
			<td><strong> Student ID</strong></td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getStudent_id())%></td>


			<td><strong>Branch</strong></td>
			<td style="font-size: 70%; font-weight: normal;"><%=StringFormatter.getDisplayString(student.getProgram_allocated())%></td>
	</table>

	<p class="p0" style="font-weight: bold;">PLEASE USE BLOCK
		LETTERS</p>
	<br />

	<table class="tabl3" style="table-layout: fixed">
		<tr>
			<td>Student Name (In English)</td>

			<td colspan="3" style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getName())%></td>

		</tr>
		<tr>
			<td>Student Name (In Hindi)</td>
			<td colspan="3"></td>
		</tr>

		<tr>
			<td>Father's Name (In English)</td>
			<td colspan="3" style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getFather_name())%></td>
		</tr>

		<tr>
			<td>Father's Name (In Hindi)</td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td>Email</td>
			<td style="font-weight: normal" colspan="3"><%=StringFormatter.getDisplayString(student.getEmail()).toLowerCase()%></td>
			</tr>

		<tr>
			<td>Date of Birth</td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getDate_of_birth())%></td>
			<td>Nationality</td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getNationality())%></td>
		</tr>

		<tr>
			<td>Gender</td>
			<td><%=StringFormatter.getDisplayString(student.getGender())%></td>
			<td>Mobile no</td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getMobile())%></td>
		</tr>
		<tr>
			<td>Blood group</td>
			<td></td>
			<td>Aadhar no. of student</td>
			<td></td>
		</tr>

		<tr>
			<td>Admitted under category</td>
			<td></td>
			<td>Physical Disabled</td>
			<td><%=StringFormatter.booleanToString(student.isPwd())%></td>
		</tr>

		<tr>
			<td>Name of Programme:</td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getProgram_allocated())%></td>
			<td>Day Scholar / Hosteller</td>
			<td></td>
		</tr>

		<tr>
			<td>Address for communication</td>
			<td colspan="3"><%=StringFormatter.getDisplayString(student.getPermanent_address())%></td>
		</tr>
		<tr>
			<td>Street</td>
			<td></td>
			<td>City</td>
			<td></td>


		</tr>
		<tr>
			<td>District</td>
			<td></td>
			<td>Pin code</td>
			<td></td>



		</tr>
		<tr>
			<td>State</td>
			<td style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getState_eligibility())%></td>
			<td>Country</td>
			<td style="font-weight: normal">India</td>
		</tr>

		<tr>
			<td>Phone No.(res)</td>
			<td></td>
			<td>Office</td>
			<td></td>
		</tr>
		<tr>
			<td>Permanent Address</td>
			<td colspan="3" style="font-weight: normal"><%=StringFormatter.getDisplayString(student.getPermanent_address())%></td>
		</tr>

		<tr>
			<td>Guardian(if any)Name:</td>
			<td><%=StringFormatter.getDisplayString(student.getGuardian_name())%></td>

			<td>Relationship with Student</td>
			<td></td>
		</tr>



		<tr>
			<td>Guardian Address</td>
			<td colspan="3"><%=StringFormatter.getDisplayString(student.getGuardian_address())%></td>
		</tr>


	</table>
	<p style="page-break-after:always;" ></p>
	
	<table class="tabl3">
		<tr>
			<td>S.No</td>
			<td>Degree</td>
			<td>Discipline</td>
			<td>Board/Inst./Univ.</td>
			<td>Year</td>
			<td>%Marks/CGPA/Degree</td>
		</tr>

		<tr>
			<td>1.</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>2.</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>3.</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>4.</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>5.</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>

	</table>

	<p>
		<strong>Declaration</strong>
	</p>

	<ul>
		<li>I am eligible for registration</li>
		<li>I do hereby agree to abide by all the Ordinances/statues and
			regulations of the institute enforced from time to time</li>
		<li>I do certify that entries made by me in this form are correct
			to the best of my knowledge</li>
		<li>I hereby solemnly declare that I will maintain good conduct
			throughout my stay in this institute</li>
		<li>I understand that the institute reserves the right to cancel
			my admission at any time during my stay at this institute if it is in
			interest of the institute to do so</li>
		<li>I understand that if any document is found forged at any
			stage during my course of study, will automatically stand cancelled
			and legal action would be taken against me</li>
		<li>I have submitted all the documents listed in the below table</li>
	</ul>

	<table class="tabl"
		style="border: 1px solid black; border-collapse: collapse;">
		<tr>
			<th width="10%">S. No.</th>
			<th width="50%">Particular</th>
			<th width="20%">Verified</th>
			<th width="20%">Pending</th>
		</tr>
		<tr>
			<td>1.</td>
			<td>Provisional Allotment letter issued by CSAB/JOSSA</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>SAT score proof in hard copy(print out)/JEE Mains score
				card/Rank Card</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Date of birth certificate (X class pass
				certificate/marksheet)</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Marksheets ,certificate & Degree of qualifying examination
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Character certificate from the last school or institute
				attended in original</td>
			<td></td>
			<td></td>
		<tr>
			<td>6.</td>
			<td>Transfer certificate/Migration Certificate submitted in
				original</td>
			<td></td>
			<td></td>
		<tr>
			<td>7.</td>
			<td>Medical certificate issued by recognized/reputed hospital</td>
			<td></td>
			<td></td>
		<tr>
			<td>8.</td>
			<td>Caste Certifiacte(SC/ST/OBC)</td>
			<td></td>
			<td></td>
		<tr>
			<td>9.</td>
			<td>Certificate of physical handicapped(if applicable)</td>
			<td></td>
			<td></td>
		<tr>
			<td>10.</td>
			<td>ID Proof In case of Indian student-Aadhaar card/voter ID
				card</td>
			<td></td>
			<td></td>
		<tr>
			<td>11.</td>
			<td>Gap certificate</td>
			<td></td>
			<td></td>
		<tr>
			<td>12.</td>
			<td>Income certificate from competent authority</td>
			<td></td>
			<td></td>
	</table>
	<br>
	<table class="table">
		<tr>
			<td style="border: none !important;"><b>Checked
					by.................................................................&nbsp;
					&nbsp;
					<td style="border: none !important;"><b>Verified
							by.................................................................
							<tr style="border: none;">
								<td style="border: none !important;">Name
									.................................................
								<td style="border: none !important;">Name.........................................................................

								
							<tr style="border: none;">
								<td style="border: none !important;">
								<td style="border: none !important;">Officer Incharge
	</table>
<script>window.print()</script>
</body>
</html>