<html>
<head>
<style>
tr, td, th {
	border: 1px solid black;
}
</style>
</head>
<body>
	<p align="center">
		<img alt=""
			src="../image/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>



	
		<h3 style="text-align:center">Documents Checklist</h3>
	
	<table style="border: none;!important">
		<tr>
			<td style="border: none;!important">Student Name:</td>
			<td style="border: none;!important">_______________________</td>
		</tr>
		<tr style="border: none;!important">
			<td style="border: none;!important">JEE Main Roll Number:</td>
			<td style="border: none;!important">_______________________</td>
		</tr>

	</table>
	<br/>
	<table style="border: 1px solid black; border-collapse: collapse;">
		<tr>
			<th width="10%">S. No.</th>
			<th width="50%">Particular</th>
			<th width="20%">Verified</th>
			<th width="20%">Pending</th>
		</tr>
		<tr>
			<td>1.</td>
			<td>Provisional Allotment letter issued by CSAB/JOSSA</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>SAT score proof in hard copy(print out)/JEE Mains score
				card/Rank Card</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Date of birth certificate (X class pass
				certificate/marksheet)</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>3.</td>
			<td>Income Certificate</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Marksheets ,certificate &amp; Degree of qualifying
				examination</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Character certificate from the last school or institute
				attended in original</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>6.</td>
			<td>Transfer certificate/Migration Certificate submitted in
				original</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>7.</td>
			<td>Medical certificate issued by recognized/reputed hospital</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>8.</td>
			<td>Caste Certificate(SC/ST/OBC)</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>9.</td>
			<td>Certificate of physical handicapped(if applicable)</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>10.</td>
			<td>ID Proof In case of Indian student-Aadhaar card/voter ID card</td>
			<td></td>
			<td></td></tr>
		<tr>
			<td>11.</td>
			<td>Gap certificate</td>
			<td></td>
			<td></td></tr>
	</table>
	<br/>
	<table>
		<tr>
			<td style="border: none !important;"><b>Checked
					by.................................................................</b></td>
			<td style="border: none !important;"><b>Verified
					by.................................................................</b></td>
					</tr>
		<tr style="border: none;">
			<td style="border: none !important;">Name
				.................................................</td>
			<td style="border: none !important;">Name.........................................................................
			</td>
		</tr>
		<tr style="border: none;">
			<td style="border: none !important;"></td>
			<td style="border: none !important;">Officer Incharge</td>
		</tr>
	</table>
	<h2>Steps completed</h2>
	<table >
	<tr><td width="20%">1.</td><td width="60%">Physical Reporting</td><td width="20%"><input type="checkbox" class="form-control" style="height:30px;width:30px"/></td></tr>
	<tr><td width="20%">2.</td><td width="60%">Documents Uploaded</td><td width="20%"><input type="checkbox" class="form-control" style="height:30px;width:30px"/></td></tr>
	<tr><td width="20%">3.</td><td width="60%">Data Updated</td><td width="20%"><input type="checkbox" class="form-control" style="height:30px;width:30px"/></td></tr>
	<tr><td width="20%">4.</td><td width="60%">Data Verified</td><td width="20%"><input type="checkbox" class="form-control" style="height:30px;width:30px"/></td></tr>
	<tr><td width="20%">5.</td><td width="60%">Payment Challan Generated</td><td width="20%"><input type="checkbox" class="form-control" style="height:30px;width:30px"/></td></tr>
	
	</table>
</body>
</html>
