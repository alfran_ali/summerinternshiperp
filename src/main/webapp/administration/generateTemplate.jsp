<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="../plugins/select2/select2.min.css">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Office <small>Home</small>

				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>

			</section>


				
				<input type="hidden"
					id="file_id" name="file_id" value="<%=request.getParameter("file_id")%>" />
					 <button onclick="getStudent()" class=btn btn-primary>Generate Document</button>
			<label>Student Id</label>
			 <select id="student_list"
				class="form-control select2" multiple="multiple"
				data-placeholder="Begin typing a student id or name" style="width: 100%;">
				
			</select>

			<!-- Main content -->
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>

	</div>
	<!-- ./wrapper -->
<script type="text/javascript">
function getStudent(){
	var students=[];
	for(i=0;i<$("#student_list").select2('data').length;i++){
	//alert(JSON.stringify($("#user_list").select2('data')[i]));
	students.push($("#student_list").select2('data')[i].student_id);
	}
	alert(students);
	return false;
} 
$("#student_list").select2({
    ajax: {
      url: "../AutoSuggest?domain=students",
      dataType: 'json',
      delay: 250,
      type:"POST",
      data: function (params) {
        return {
          string: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        // !IMPORTANT! your every item in data.items has to have an .id property - this is the actual value that Select2 uses
        // Luckily the source data.items already have one
        return {
          results: data.items,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup; // let our custom formatter work
    },
    minimumInputLength: 1,
    templateResult: function(repo) {
      if (repo.loading) return repo.text;
      return repo.full_name;
    },
    templateSelection: function(repo) {
      return repo.student_id+" ("+repo.full_name+")" || repo.text;
    }
  });




</script>
	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<script src="../plugins/select2/suggestion.js"></script>

	<script src="../plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">
function getStudent(){
	var students=[];
	for(i=0;i<$("#student_list").select2('data').length;i++){
	//alert(JSON.stringify($("#user_list").select2('data')[i]));
	students.push($("#student_list").select2('data')[i].student_id);
	}
	var file_id=document.getElementById("file_id").value;
	
	    window.open("../GenerateDocument?student_id="+students+"&file_id="+file_id,"_blank");
	

	
	return false;


}

$("#student_list").select2({
    ajax: {
      url: "../AutoSuggest?domain=students",
      dataType: 'json',
      delay: 250,
      type:"POST",
      data: function (params) {
        return {
          string: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        // !IMPORTANT! your every item in data.items has to have an .id property - this is the actual value that Select2 uses
        // Luckily the source data.items already have one
        return {
          results: data.items,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup; // let our custom formatter work
    },
    minimumInputLength: 1,
    templateResult: function(repo) {
      if (repo.loading) return repo.text;
      return repo.full_name;
    },
    templateSelection: function(repo) {
      return repo.student_id+" ("+repo.full_name+")" || repo.text;
    }
  });




</script>
	<!-- jQuery 2.1.4 -->

</body>
</html>