package actions.feePayment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.Query;
import postgreSQLDatabase.feePayment.Transaction;

/**
 * Servlet implementation class AddPaymentInfo
 */

public class AddOfficePaymentData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddOfficePaymentData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// challan 5
		HttpSession session = request.getSession();
		int transaction_id = Integer.parseInt(request.getParameter("transaction_id"));
		
		boolean refund=Boolean.parseBoolean(request.getParameter("refund"));
		
			Transaction transaction = postgreSQLDatabase.feePayment.Query.getFeeTransactionById(transaction_id);
			long reg_id = transaction.getReg_id();
			int sem=transaction.getSemester();
		if (request.getParameter("payment_method").toString().equals("demand_draft")) {
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("dd_no", request.getParameter("dd_no"));
			details.put("dd_comment", request.getParameter("dd_comment"));
			Long ref_id = 0L;
		
if(refund)				
ref_id = Query.addFeePayment("DD payment", 5, details, -Long.parseLong(request.getParameter("amount")),
						reg_id,reg_id,transaction_id);
else
	ref_id = Query.addFeePayment("DD payment", 5, details, Long.parseLong(request.getParameter("amount")),
			reg_id,reg_id,transaction_id);	
			if(sem==1)	postgreSQLDatabase.registration.Query.updateVerificationStatus(3, reg_id);
		
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());

		}

		if (request.getParameter("payment_method").equals("neft")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("ifsc", request.getParameter("�fsc"));
			details.put("date", request.getParameter("date"));
			details.put("tid", request.getParameter("tid"));
			Long ref_id = 0L;
			
	if(refund)			ref_id = Query.addFeePayment("NEFT payment", 4, details, -Long.parseLong(request.getParameter("amount")),
						reg_id,	reg_id,transaction_id);
				ref_id = Query.addFeePayment("NEFT payment", 4, details, Long.parseLong(request.getParameter("amount")),
						reg_id,	reg_id,transaction_id);
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
		if (request.getParameter("payment_method").equals("challan")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", Long.parseLong(request.getParameter("amount")));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("challan_comment", request.getParameter("challan_comment"));
		Long ref_id = 0L;
			
	if(refund)			ref_id = Query.addFeePayment("Challan payment", 2, details,
						-Long.parseLong(request.getParameter("amount")),
						reg_id, reg_id,transaction_id);
				ref_id = Query.addFeePayment("Challan payment", 2, details,
						Long.parseLong(request.getParameter("amount")),
						reg_id, reg_id,transaction_id);
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
		if (request.getParameter("payment_method").equals("refund")) {
			
			JSONObject details = new JSONObject();
			details.put("josaa_amount", Long.parseLong(request.getParameter("josaa_amount")));
			details.put("fee_amount", request.getParameter("fee_amount"));
			details.put("refund_amount", request.getParameter("amount"));
			details.put("comment", "Amount to be balanced in fees of next semester");
		Long ref_id = 0L;
			
	if(refund)			ref_id = Query.addFeePayment("Refund", 7, details,
						Long.parseLong(request.getParameter("refund_amount")),
						reg_id, reg_id,transaction_id);
	ref_id = Query.addFeePayment("Refund", 7, details,
			Long.parseLong(request.getParameter("amount")),
			reg_id, reg_id,transaction_id);
	
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
	}

}
