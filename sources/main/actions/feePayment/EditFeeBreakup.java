package actions.feePayment;

import java.io.IOException;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import postgreSQLDatabase.feePayment.FeeBreakup;

/**
 * Servlet implementation class EditFeeBreakup
 */
@WebServlet("/EditFeeBreakup")
public class EditFeeBreakup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditFeeBreakup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer = response.getWriter();
		int semester = Integer.parseInt(request.getParameter("semester"));
		int year = Integer.parseInt(request.getParameter("year"));
        ArrayList<FeeBreakup> list = postgreSQLDatabase.feePayment.Query.getFeeBreakup(semester, year);
	    Iterator<FeeBreakup> iterator = list.iterator();
	    JSONObject j_object = new JSONObject();
	    while(iterator.hasNext()){
	    	FeeBreakup current = iterator.next();
	    	j_object.put(current.getCategory(),current.getBreakup());
	    }
	    writer.write(j_object.toString());
	}

}
