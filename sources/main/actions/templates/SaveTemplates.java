package actions.templates;

import users.User;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
//import postgreSQLDatabase.file;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import exceptions.IncorrectFormatException;
import exceptions.TagNotFoundException;
import postgreSQLDatabase.templates.Tags;

/**
 * Servlet implementation class SaveTemplates
 */
@MultipartConfig
@WebServlet("/SaveTemplates")
public class SaveTemplates extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveTemplates() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> formItems = null;
			try {
				formItems = upload.parseRequest(request);
			} catch (FileUploadException e4) {
				// TODO Auto-generated catch block
				e4.printStackTrace();
			}
		
		System.out.println(formItems.size());
		String file = null;
		String title=null;
		Iterator<FileItem> iter = formItems.iterator();
		while (iter.hasNext()) {
			FileItem item = (FileItem) iter.next();
			if (item.isFormField()) {

				try{
					switch(item.getFieldName()){


					case "title" :
						title=item.getString();
						
						break;
					case "file":
						file=item.getString();
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
					
				}	}
		
			}
		
		System.out.println(title);
		System.out.println(file);
		
		
		
		
		
		
		if (file != null) {
			try {
				//String file = request.getParameter("contents");
				//String title=request.getParameter("name");
				System.out.println(file);
				//System.out.println(request.getParameter("name"));
				
				String uploadFilePath = "templates";
				// System.out.println(uploadFilePath);
				ftp.FTPUpload.createDirectory(uploadFilePath);
				String filePath = uploadFilePath +"/" + title+".txt";
 
				postgreSQLDatabase.file.File obj = new postgreSQLDatabase.file.File();
			
				obj.setFile_name(title+".txt");
				
				postgreSQLDatabase.file.Query.getFileId(title);

				obj.setAuthor(Long.parseLong(session.getAttribute("erpId").toString()));
				obj.setDirectory("templates");
				obj.setExtension(".txt");
				System.out.println(filePath);
				ByteArrayInputStream upload_stream = new ByteArrayInputStream(file.getBytes(StandardCharsets.UTF_8));
				ftp.FTPUpload.uploadFile(upload_stream,filePath);
				upload_stream.close();
				long file_id=	postgreSQLDatabase.file.Query.addNewFile(obj);
				ArrayList<String> tags = new ArrayList<String>();
				tags = email.Mapping.findTags(file);
				Iterator<String> iterator = tags.iterator();

				while (iterator.hasNext()) {
					String tagname=iterator.next();
					Tags t = postgreSQLDatabase.templates.Query.getTags(tagname);
					if(t==null){
						TagNotFoundException e=new TagNotFoundException(tagname);
						throw e;
					} 
					
				}

					postgreSQLDatabase.templates.Query.addTemplate(title, file_id,Long.parseLong(session.getAttribute("erpId").toString()), tags);

				}

			 catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			catch (TagNotFoundException e3){
				e3.printStackTrace();

			}

		}

	}

}
