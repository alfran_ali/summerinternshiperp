package actions.registration;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;

import postgreSQLDatabase.file.Query;
import users.Student;

/**
 * Servlet implementation class UploadProfilePicture
 */
@WebServlet("/UploadIDCardImage")
@MultipartConfig
public class UploadIDCardImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadIDCardImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Long erp_id=	Long.parseLong(request.getSession().getAttribute("erpId").toString());
		Long student_reg_id = null;
	    DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> formItems=null;
		try {
			formItems = upload.parseRequest(request);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
		

		Iterator<FileItem> iterator = formItems.iterator();
			while (iterator.hasNext()) {
				FileItem file_item = iterator.next();
		
				Iterator<FileItem> iter1 = formItems.iterator();
				while (iter1.hasNext()) {
					FileItem item = (FileItem) iter1.next();
					if (item.isFormField()) {
						switch(item.getFieldName()){
						case "reg_id" :
						student_reg_id	=Long.parseLong(item.getString());
							break;
						}
						}
				}
				
				
				
				
				
				
				String file = "image_" + String.valueOf(student_reg_id)+".png";
				String filePath = "id_card_images" ;
				ftp.FTPUpload.createDirectory("id_card_images");
				
				
				
				
				
				Iterator<FileItem> iter = formItems.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (item.isFormField()) {
						switch(item.getFieldName()){
						case "data" :
							String imageBase64=	item.getString();
							 imageBase64=	imageBase64.substring(imageBase64.indexOf(",")+1);
							 postgreSQLDatabase.file.File file_wrapper = new postgreSQLDatabase.file.File();
							 file_wrapper.setDirectory(filePath);
								file_wrapper.setAuthor(Long.parseLong((request.getSession().getAttribute("erpId")).toString()));
								String name[] = file.split("\\.");
								System.out.println(file);
								file_wrapper.setExtension(name[1]);
								file_wrapper.setFile_name(file);
							long file_id = 0;
							try {
								file_id = postgreSQLDatabase.file.Query.addNewFile(file_wrapper);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								ftp.FTPUpload.uploadFile(  utilities.ServletUtils.getDecodedBase64Stream(imageBase64), filePath + "/" + file);
							break;
						}
						}
				}
			}

		

	
			
	    
	}
}


