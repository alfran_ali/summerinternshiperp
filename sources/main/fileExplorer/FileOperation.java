/**
 * 
 */
package fileExplorer;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.http.Part;

/**
 * @author Shubhi
 *
 */
public class FileOperation {
	public static void main(String[] args) {
		long file_id;
		//file_id=uploadFile("","");	
	}

	/**
	 * @param string
	 * @param string2
	 * @param string3
	 * @throws IOException 
	 */
	private static Long uploadFile(Part part,String filename, String directory) throws IOException {
			long date=new Date().getTime();
			String time=String.valueOf(date);
			System.out.println(time);
			String file=filename+time;
			part.write(directory + File.separator + file);
    
		return null;
	}

	private static String getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		System.out.println("content-disposition header= "+contentDisp);
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				return token.substring(token.indexOf("=") + 2, token.length()-1);
			}
		}
		return "";
	}

}
