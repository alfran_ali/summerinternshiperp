/**
 * 
 */
package postgreSQLDatabase.feePayment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;
import settings.database.PostgreSQLConnection;

/**
 * @author manisha pc
 *
 */
public class RegistrationFeePaymentQuery {
	private static PreparedStatement proc;
	
	public static JSONArray getRegistrationFeeJson(Long reg_id) {

		try {
			proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getRegistrationFeeJson\"(?);");
			proc.setLong(1, reg_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray j_array = new JSONArray();
			ResultSet as = rs.getArray(1).getResultSet();
			while (as.next()) {
				JSONObject current = new JSONObject(as.getString(2));
				j_array.put(current);
			}
			return j_array;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	public static void addRegistrationFeeBreakup(int round, String category, String breakup, int year) {
		try {

			JSONArray fee_breakup = new JSONArray(breakup);
			JSONObject amt_obj = fee_breakup.getJSONObject(fee_breakup.length() - 1);
			JSONObject j_array[] = new JSONObject[fee_breakup.length()];
			for (int i = 0; i < fee_breakup.length(); i++) {
				j_array[i] = fee_breakup.getJSONObject(i);

			}
			int amount = amt_obj.getInt("total");
			// System.out.println(amount);
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addRegistrationFeeBreakup\"(?,?,?,?,?);");
			proc.setInt(1, year);
			proc.setInt(2, round);
			proc.setString(3, category);
			proc.setObject(4, PostgreSQLConnection.getConnection().createArrayOf("json", j_array));
			proc.setInt(5, amount);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void addRegistrationFeeTransaction(int round, String category, long reg_id, int year) {

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addRegistrationFeeTransaction\"(?,?,?,?);");

			proc.setInt(1, round);

			proc.setString(2, category);

			proc.setLong(3, reg_id);
			proc.setInt(4, year);
			System.out.println(proc);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @param comment
	 * @param pay_method
	 * @param details
	 * @param amt
	 * @param reg_id
	 * @param payee
	 * @param beneficiary
	 * @param semester
	 * @return
	 */
	public static Long addFeePayment(String comment, int pay_method, JSONObject details, long amt, long payee,
			long beneficiary, long transaction_id) {
//TODO
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addRegistrationFeePayment\"(?,?,?,?,?,?,?);");

			proc.setString(1, comment);
			proc.setInt(2, pay_method);
			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(details.toString());

			proc.setObject(3, jsonObject);

			proc.setLong(4, amt);
			proc.setLong(5, payee);
			proc.setLong(6, beneficiary);
			proc.setLong(7, transaction_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();

			return rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0L;

	}

	public static ArrayList<FeeBreakup> getRegistrationFeeBreakup( int round, int year) {

		ArrayList<FeeBreakup> getList = new ArrayList<FeeBreakup>();
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getRegistrationFeeBreakup\"(?,?);");
			
			proc.setInt(1, round);
			proc.setInt(2, year);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {
				FeeBreakup feebreakup = new FeeBreakup();
				feebreakup.setCategory(rs.getString("category"));
				feebreakup.setRound(rs.getInt("round"));
				feebreakup.setYear(rs.getInt("year"));
				feebreakup.setTotal_amt(rs.getInt("total_amt"));
				JSONArray breakup = new JSONArray();
				ResultSet as = rs.getArray("breakup").getResultSet();
				while (as.next()) {
					JSONObject json_object = new JSONObject(as.getString(2));
					breakup.put(json_object);
				}
				feebreakup.setBreakup(breakup);
				getList.add(feebreakup);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getList;
	}
   public static void main(String[] args) {
	
}
	public static ArrayList<Transaction> getRegistrationFeeTransactions(Long reg_id) {
		ArrayList<Transaction> transcation_list = null;
		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getRegistrationFeeTransactions\"(?);");
			proc.setObject(1, reg_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);

			transcation_list = new ArrayList<Transaction>();
			while (rs.next()) {
				Transaction current = new Transaction();
				current.setCategory(rs.getString("category"));
				current.setCompleted(rs.getBoolean("completed"));
				current.setDate(rs.getDate("date"));
				current.setRound(rs.getInt("round"));
				current.setYear(rs.getInt("year"));
				current.setTransaction_id(rs.getLong("transaction_id"));
				// System.out.println(rs.getLong("transaction_id"));
				transcation_list.add(current);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (transcation_list);

	}

	public static long getRegistrationTransactionId(int year, int round, long reg_id)
			throws SQLException, IncorrectFormatException {
		long tid = 0l;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getRegistrationTransactionId\"(?,?,?);");

			proc.setInt(1, year);
			proc.setInt(2, round);

			proc.setLong(3, reg_id);

			ResultSet rs = proc.executeQuery();
			rs.next();
			tid = rs.getLong(1);

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tid;
	}

	public static ArrayList<Payment> getRegistrationFeePaymentByTransaction(Long transaction_id) {
		ArrayList<Payment> payment_list = null;

		try {
			proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getRegistrationFeePaymentByTransaction\"(?);");
			proc.setObject(1, transaction_id);
			ResultSet rs = proc.executeQuery();
			// System.out.println(proc);
			payment_list = new ArrayList<Payment>();
			while (rs.next()) {
				Payment current = new Payment();
				current.setAmount(rs.getLong("amount"));
				current.setComment(rs.getString("comment"));
				current.setPayment_method(rs.getInt("payment_method"));
				current.setVerified(rs.getBoolean("verified"));
				JSONObject j_object = new JSONObject(rs.getString("details"));
				 current.setDetails(j_object);
		        current.setRef_no(rs.getLong("ref_no"));

		payment_list.add(current);
					}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				}

		return payment_list;

	}
}
