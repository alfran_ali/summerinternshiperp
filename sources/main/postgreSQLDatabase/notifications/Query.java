/**
 * 
 */
package postgreSQLDatabase.notifications;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.json.JSONException;

import settings.database.PostgreSQLConnection;

/**
 * @author Anshula
 *
 */
public class Query {
	public static void main(String[] args) {
		System.out.println(getUnreadNotifications(1000000157L).size());
	}

	public static void addNotification(Notifications notif,Long userid){
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addNotification\"(?,?,?,?,?,?);");
			proc.setString(1,notif.getType());
			proc.setString(2,notif.getMessage());
			proc.setString(3,notif.getLink());
			proc.setDate(4,notif.getTimestamp());
			proc.setDate(5,notif.getExpiry());
			proc.setLong(6,userid);
			proc.executeQuery();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void addGroupNotification(Notifications notif,ArrayList<Long> users_id){
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addGroupNotification\"(?,?,?,?,?,?,?);");
			proc.setString(1,notif.getType());

			proc.setString(2,notif.getMessage());
			proc.setString(3,notif.getLink());
			proc.setString(4, notif.getAuthor());
			proc.setDate(5,notif.getTimestamp());
			proc.setDate(6,notif.getExpiry());
			proc.setArray(7,PostgreSQLConnection.getConnection().createArrayOf("bigint", users_id.toArray()));

			proc.executeQuery();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addGroupNotification\"(?,?,?,?,?,?,?);");
	proc.setString(1,request.getParameter("notif_type"));
	proc.setString(2,request.getParameter("msg"));
	proc.setString(3,request.getParameter("link"));
	proc.setString(4,request.getParameter("notif_author"));
	proc.setTimestamp(5, request.getParameter());
	proc.setTimestamp(6, request.getParameter(arg0));
	proc.setString(7,request.getParameter(arg0))
	//current.setTimestamp(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS")
	//.parse(current_object.getString("notif_timestamp")).getTime()));
	proc.setLong(2,Long.parseLong(request.getSession().getAttribute("erpId").toString()));
	proc.setInt(3,67);


	//System.out.println(proc.toString());
	 proc.executeQuery();

	 */


	public static void markAsRead(Long userid,Long notif_id){
		PreparedStatement proc;
		try {
			proc =PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"readNotification\"(?,?);");
			proc.setLong(1,userid);
			proc.setLong(2,notif_id);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void broadcastNotification(Notifications notif,int userid[]){
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"broadcastNotification\"(?,?,?,?,?,?);");
			proc.setString(1,notif.getType());
			proc.setString(2,notif.getMessage());
			proc.setString(3,notif.getLink());
			proc.setDate(4,notif.getTimestamp());
			proc.setDate(5,notif.getExpiry());
			//proc.setArray(6,userid);
			proc.executeQuery();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static ArrayList<Notifications> getUnreadNotifications(Long erpId){
		ArrayList<Notifications> notifications = new ArrayList<Notifications>();
		try{
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getUnreadNotifications\"(?);");
			proc.setLong(1,erpId);
			ResultSet rs = proc.executeQuery();


			while (rs.next()) {
				Notifications current = new Notifications();
				current.setId(rs.getInt("notif_id"));
				current.setType(rs.getString("notif_type"));
				current.setMessage(rs.getString("message"));
				current.setLink(rs.getString("link"));
				try {
					current.setTimestamp(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
							.parse(rs.getString("notif_timestamp")).getTime()));
					current.setExpiry(new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
							.parse(rs.getString("expiry")).getTime()));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				notifications.add(current);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return notifications;

	}
}
